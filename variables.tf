variable "access_key" {}
variable "secret_key" {}
variable "region" {}
variable "gitlab_ami" {}
variable "instance_type" {}
variable "availability_zone" {}
variable "subnet_id" {}
variable "volume_type" {}
variable "ebs_vol_size" {}
variable "security_groups" {default = []}
variable "key_name" {}
variable "tag_cc" {}
variable "tag_division" {}
variable "tag_environment" {}
variable "tag_management" {}
variable "tag_name" {}
variable "tag_owner" {}
variable "tag_project" {}
variable "runner_project_token" {}
variable "runner_executor" {}
variable "runner_run_untagged" {}
variable "runner_locked" {}
variable "runner_tag" {}
variable "new_relic_license" {}
variable "monitor_type" {}
variable "ebs_vol_name" {}