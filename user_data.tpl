#!/bin/bash -xe

########### DEFAULT ################
yum update -y

mkdir /tmp/ssm

# curl https://amazon-ssm-us-east-1.s3.amazonaws.com/latest/linux_amd64/amazon-ssm-agent.rpm -o /tmp/ssm/amazon-ssm-agent.rpm

# yum install -y /tmp/ssm/amazon-ssm-agent.rpm

/bin/hostname ${tag_name}

/bin/grep -v HOSTNAME /etc/sysconfig/network > /tmp/sysconfig_network

echo HOSTNAME=`hostname` >> /tmp/sysconfig_network

\cp /tmp/sysconfig_network /etc/sysconfig/network

if [ `cat /etc/issue | head -1 | awk '{print ${tag_name}}'` = RHEL ]
then
    echo `hostname` > /etc/hostname
    echo "preserve_hostname: true" >> /etc/cloud/cloud.cfg
elif [ `cat /etc/issue | head -1 | awk '{print ${tag_name}}'` = Amazon ]
then
    yum install -y awslogs
    service awslogs start
    chkconfig awslogs on
fi

########### JAVA 8 INSTALL ###########
yum list java-1.7*
yum remove -y java-1.7.0-openjdk
yum install -y java-1.8.0-openjdk
yum install -y java-1.8.0-openjdk-devel 
java -version


########### NEW RELIC INFRASTRUCTURE MONITORING AGENT ###########
echo "license_key: ${new_relic_license}" | tee -a /etc/newrelic-infra.yml
cat /etc/os-release
curl -o /etc/yum.repos.d/newrelic-infra.repo https://download.newrelic.com/infrastructure_agent/linux/yum/el/6/x86_64/newrelic-infra.repo
yum -q makecache -y --disablerepo='*' --enablerepo='newrelic-infra'
yum install newrelic-infra -y

########### CLOUD FOUNDRY CLI INSTALL ###########
wget -O /etc/yum.repos.d/cloudfoundry-cli.repo https://packages.cloudfoundry.org/fedora/cloudfoundry-cli.repo
yum install -y cf-cli

########### POSTGRESQL CLI INSTALL ###########
# yum install -y postgresql95

########### NODE & ANGULAR INSTALL ###########
# yum install -y gcc-c++ make
# curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash

# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

# nvm install --lts

# curl --silent --location https://rpm.nodesource.com/setup_8.x | bash -
# yum -y install nodejs
# node -v
# npm -v 
# npm install -g @angular/cli

########### RUNNER SETUP ###########
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.rpm.sh | /bin/bash

yum install -y gitlab-ci-multi-runner

gitlab-runner register --non-interactive -u https://gitlab.sealedair.com \
--tag-list '${runner_tag}' \
--registration-token '${runner_project_token}' \
--name '${runner_tag}' \
--executor '${runner_executor}'

########### SUDOERS SETUP ###########
printf 'provisioner ALL = NOPASSWD: ALL\n' > /etc/sudoers.d/provisioner && chmod 0440 /etc/sudoers.d/provisioner
printf 'gitlab-runner ALL = NOPASSWD: ALL\n' > /etc/sudoers.d/gitlab-runner && chmod 0440 /etc/sudoers.d/gitlab-runner
printf 'Defaults:gitlab-runner !requiretty\n' >> /etc/sudoers.d/gitlab-runner
