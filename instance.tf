data "template_file" "file_import" {
  template = "${file("user_data.tpl")}"

  vars {
    runner_tag           = "${var.runner_tag}"
    tag_name             = "${var.tag_name}"
    runner_project_token = "${var.runner_project_token}"
    runner_executor      = "${var.runner_executor}"
    runner_run_untagged  = "${var.runner_run_untagged}"
    runner_locked        = "${var.runner_locked}"
    new_relic_license    = "${var.new_relic_license}"
  }
}

data "template_cloudinit_config" "render_parts" {
  gzip          = true
  base64_encode = true

  part {
    filename     = "user_data.sh"
    content_type = "text/x-shellscript"
    content      = "${data.template_file.file_import.rendered}"
  }
}

resource "aws_instance" "gitlab-runner-instance" {
  ami                    = "${var.gitlab_ami}"
  instance_type          = "${var.instance_type}"
  key_name               = "${var.key_name}"
  source_dest_check      = true
  subnet_id              = "${var.subnet_id}"
  availability_zone      = "${var.availability_zone}"
  vpc_security_group_ids = "${var.security_groups}"
  user_data              = "${data.template_cloudinit_config.render_parts.rendered}"

  tags {
    CostCenter  = "${var.tag_cc}"
    Division    = "${var.tag_division}"
    Environment = "${var.tag_environment}"
    Management  = "${var.tag_management}"
    Name        = "${var.tag_name}"
    Owner       = "${var.tag_owner}"
    Project     = "${var.tag_project}"
    MonitorType = "${var.monitor_type}"
  }
}